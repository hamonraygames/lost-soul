using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [Header("Movement Section")]
    [Range(1f, 25f)] public float walkSpeed = 5f;
    [Range(1.2f, 3f)] public float runMultiplier = 2f;
    [Range(6f, 10f)] public float targetSpeed = 10f;
    [Range(1f, 10f)] public float acceleration  = 5f;
    [Range(1f, 25f)] public float jumpForce  = 5f;

    [SerializeField] public Transform groundCheck;
    public float groundDistance = 0.2f;
    public LayerMask groundLayer;

    private Rigidbody _rgbd;
    private Animator _playerAnim;
    private SpriteRenderer _spriteRenderer;
    private Vector3 _movementDir;
    private bool _isFacingRight = true;


    private void Awake() {
        targetSpeed = walkSpeed * runMultiplier;
    }
    void Start()
    {
        _rgbd = GetComponent<Rigidbody>();
        _playerAnim = GetComponentInChildren<Animator>();
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    private void Update() {
        GetAxis();
        FlipSprite();
        SetAnimation();
    }

    private void FixedUpdate() {
        Movement();
        Jump();
    }

    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded()){
            _rgbd.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }

    private void SetAnimation()
    {
        if (_rgbd.velocity == Vector3.zero)
        {
            _playerAnim.SetBool("isIdle", true);
            _playerAnim.SetBool("isWalking", false);
            _playerAnim.SetBool("isRunning", false);
        }
        else
        {
            _playerAnim.SetBool("isWalking", !IsRunning());
            _playerAnim.SetBool("isRunning", IsRunning());
        }
        _playerAnim.SetBool("isJumping", IsGrounded());
    }

    private void FlipSprite()
    {
        if (_movementDir != null) {
            if (_movementDir.x > 0) { _isFacingRight = true; _spriteRenderer.flipX  = false; }
            else if (_movementDir.x < 0) { _isFacingRight = false; _spriteRenderer.flipX  = true; }
        }
    }

    private void GetAxis()
    {
        _movementDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical")).normalized;
    }


    private void Movement()
    {
        Vector3 _movement;
        if (IsRunning()) 
        { 
            _movement = _movementDir * walkSpeed * runMultiplier; 
        }
        else
        { 
            _movement = _movementDir * walkSpeed ; 
        }

        _rgbd.velocity = new Vector3(_movement.x, _rgbd.velocity.y, _movement.z);
    }

    private bool IsRunning() => Input.GetKey(KeyCode.LeftShift);

    public bool IsGrounded() => Physics.Raycast(groundCheck.position, Vector3.down, groundDistance, groundLayer);

    private void OnDrawGizmos() {
        Gizmos.DrawLine(groundCheck.position, new Vector3(groundCheck.position.x, groundCheck.position.y - groundDistance, groundCheck.position.z));
    }


}

